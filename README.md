Test Code for Solidity
======

Includes a source code (Calculator.java) and a test code FirstTest.java.

The yaml script automatically runs the test code on a Docker image and stores 
the result on the github.  

See: https://gitlab.com/oascigil/contract-software-test/pipelines
